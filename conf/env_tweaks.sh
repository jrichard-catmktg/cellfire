#!/bin/bash

# Places environment tweaks into the conweb folder to avoid PHP errors

if [ -d ./htdocs/includes ]
	then
		cp ./../../conf/conweb_env_tweaks/properties.php ./htdocs/includes/properties.php
		cp ./../../conf/conweb_env_tweaks/utils.php ./htdocs/includes/lib/utils.php
	else
		echo "ERROR:  Make sure you run this command from the conweb directory."
fi
