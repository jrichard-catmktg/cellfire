#!/bin/bash

function go {
  sudo apt-get -y upgrade
  sudo apt-get -y update
  
  # Apache setup
  sudo apt-get -y install apache2
  sudo cp /conf/apache2.conf /etc/apache2/
  sudo cp /conf/000-default.conf /etc/apache2/sites-available

  # Vim prefs
  sudo echo "syntax off" >> ~/.vimrc
  sudo echo "set number" >> ~/.vimrc

  # mySQL setup
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

  sudo apt-get -y install mysql-server

  echo "CREATE USER 'cwebuser'@'%' IDENTIFIED BY 'ROr34d'" | mysql -uroot -proot
  echo "CREATE DATABASE conweb" | mysql -uroot -proot
  echo "GRANT ALL ON conweb.* TO 'cwebuser'@'%'" | mysql -uroot -proot
  echo "flush privileges" | mysql -uroot -proot

  if [ -f /conf/conweb_3_20_2017.sql ]; then
    mysql -ucwebuser -pROr34d conweb < /conf/conweb_3_20_2017.sql
  else
    echo "WARNING: No SQL file found, conweb database will be left empty."
  fi
  
  # Make logs directory
  if [ ! -d /www/conweb/logs ]; then
    mkdir /www/conweb/logs
  fi
  
  # set environment as dev
  # this is read by cellfire's application
  touch /isdev.true
  
  # Wrap it up!
  echo "sudo su" >> /home/vagrant/.bashrc

  sudo cp /conf/motd /etc/motd
  
  touch /conf/setup_complete

  echo ""
  echo "--------------------"
  echo "All set!"
  echo "--------------------"
  echo "Be sure to add the following entries to your hosts file:"
  echo "192.168.33.10 cellfire.local"
  echo "192.168.33.10 retailer.local"
  echo "--------------------"
  echo ""
}

# TODO:  Fix the first conditional

## Make sure Cellfire's source code is in place, and that we haven't
## run the setup before.
##
#if [ -d /www/conweb/htdocs ]; then
#  if [ -f /conf/setup_complete ]; then
#    echo ""
#    echo "Found \"setup_complete\" file in /conf, skipping bootstrap.sh"
#    echo "(Remove /conf/setup_complete if you're trying to reinstall.)"
#    echo ""
#  else
#    go
#  fi
#else 
#  echo ""
#  abort "Oops! Looks like Cellfire's source code isn't in the right place."
#  abort "Clone their repository into the /www/conweb directory, then run \"Vagrant reload\"."
#  abort "Example directory structure: /www/conweb/htdocs/..."
#  echo ""
#fi

if [ -f /conf/setup_complete ]; then
  echo ""
  echo "Found \"setup_complete\" file in /conf, skipping bootstrap.sh"
  echo "(Remove /conf/setup_complete if you're trying to reinstall.)"
  echo ""
else
  go
fi