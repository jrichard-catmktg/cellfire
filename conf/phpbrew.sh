#!/bin/bash

#install PHP 5.4.13 via PHPBrew with all extensions
apt-get -y build-dep php5
apt-get install -y php5 php5-dev php-pear autoconf automake curl build-essential libxslt1-dev re2c libxml2 libxml2-dev php5-cli bison libbz2-dev libreadline-dev
apt-get install -y libfreetype6 libfreetype6-dev libpng12-0 libpng12-dev libjpeg-dev libjpeg8-dev libjpeg8  libgd-dev libgd3 libxpm4 libltdl7 libltdl-dev
apt-get install -y libssl-dev openssl
apt-get install -y gettext libgettextpo-dev libgettextpo0
apt-get install -y php5-cli
apt-get install -y libmcrypt-dev
apt-get install -y libicu-dev

#additional dependancies
apt-get install -y php5-gd

curl -L -O https://github.com/phpbrew/phpbrew/raw/master/phpbrew
chmod +x phpbrew
mv phpbrew /usr/bin/phpbrew

# grab list of php versions
phpbrew init
phpbrew known --update
phpbrew update
phpbrew known --old
phpbrew known --more

# install correct php version
phpbrew install 5.4.13 +default +soap +mysql +apxs2=/usr/bin/apxs2

# install additional extensions
source ~/.phpbrew/bashrc
phpbrew switch php-5.4.13
phpbrew ext install gd
phpbrew ext install apc

# update .bashrc files
echo "source ~/.phpbrew/bashrc" >> ~/.bashrc
source ~/.bashrc

# bring in php.ini file
sudo cp /conf/php.ini ~/.phpbrew/php/php-5.4.13/etc/php.ini

#wrap it up!
service apache2 restart

echo ""
echo "--------------------"
echo "All set!"
echo "--------------------"
php -v
echo "--------------------"
echo ""