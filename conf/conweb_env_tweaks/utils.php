<?
/*
 * Author Jonathan Samples
 * Created February 8, 2008
 *
 * File for Utility functions
 */
/*
require_once("Mail.php");
require_once("Mail/mail.php");
require_once("Mail/mime.php");
*/

// this version of htmlspecialchars determines if the thing being checked is even set before it does anything - used by feedback form to stop errors from non posted data
function isSetHtmlSpecial($var)
{
	$retVal = '';
	if (isset($var) && !empty($var))
	{
		$retVal = htmlspecialchars($var);
	}
	return $retVal;
}

function print_r_pre($ob){
	echo "<pre style=\"text-align: left;\">";
	print_r($ob);
	echo "</pre>";
}

function make_sql_safe($str){
	return preg_replace("/'/","\'",preg_replace("/\"/","\\\"",$str));
}

function safestring($str){
	global $LOGGER;
	
	$regs = array();
	$str = preg_replace("/'/", "\'", $str);
	$str = preg_replace('/"/', "&quot;", $str);
	$str = preg_replace('/\./'.chr(153),"&trade;",$str);

	return $str;
}

function display_error(&$errorMsg) {
	$errorMessageString = "";

	if (isset($errorMsg) && !empty($errorMsg) ) {
		$errorMessageString = "&nbsp;&nbsp;<FONT CLASS = \"error\">" . $errorMsg . "</FONT>";
	}

	return $errorMessageString;
}

function display_error_without_space(&$errorMsg) {
	$errorMessageString = "";

	if (isset($errorMsg) && !empty($errorMsg) ) {
		$errorMessageString = "<FONT CLASS = \"error\">" . $errorMsg . "</FONT>";
	}

	return $errorMessageString;
}

function display_success($successMsg) {
	$successMessageString = "";

	if (isset($successMsg) && !empty($successMsg) ) {
		$successMessageString = "&nbsp;&nbsp;<FONT CLASS = \"success\">" . $successMsg . "</FONT>";
	}

	return $successMessageString;
}

function strip_html_tags($html_string) {
	$text_string = strip_tags($html_string, "<br><a>");

	$order   = array("<BR />", "<br />", "<BR>", "<br>", "<BR/>", "<br/>");
	$replace = '\r\n';
	$text_string = str_replace($order, $replace, $text_string);
	return $text_string;
}

function check_email_address($email) 
{
	return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}


/*
 *	isPhoneBrowser
 *
 *	@return 1 if we think the browser hitting the site is on a phone, 0 if we think it's a PC

 current thinking is to try and prove the browser is a phone
 - if it says it supports wap (for text/ application/ image/ or whatever), we say 'phone!'
 - if its user agent doesn't start with Mozilla or Opera, we say 'phone!'
 - if its user agent does start with Mozilla or Opera, but we can also find Windows CE,
 Smartphone etc. in there too, we say 'phone!'
 
 NOTE!!!! If you're updating this routine, update the one in vzwhtdocs too!!!!!!!

 *
 *
 */

if (!function_exists('getallheaders'))
{
	//nginx
	function getallheaders()
	{
		$allHeaders = '';
		foreach ($_SERVER as $name => $value)
		{
			if (substr($name, 0, 5) == 'HTTP_')
			{
				$allHeaders[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $allHeaders;
	}
} else {
	//apache
}


function isPhoneBrowser()
{
	// first, see if the browser can accept wap content
	$returnVal = (isset($_SERVER['HTTP_ACCEPT']) && strstr($_SERVER['HTTP_ACCEPT'],'/vnd.wap'));
	// if it doesn't claim it can accept wap
	if (!$returnVal)
	{
		// just 'cos it says it can't understand wap or wml, doesn't mean it's not a phone
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
		$allHeaders = getAllHeaders();
		// if the user agent claims to support Mozilla or Opera, see if it might still be a phone
		if (preg_match('/^Mozilla/', $userAgent) || preg_match('/^Opera/', $userAgent))
		{
			// assume all Mozilla/Opera compatible browsers are not phones unless one of the following matches
			$returnVal =
			(
			// see if it is running on CE (the UA-OS section relates to CMI-4758)
			preg_match('/Windows CE/', $userAgent) || (isset($allHeaders['UA-OS']) && preg_match('/Windows CE/', $allHeaders['UA-OS'])) ||
			// see if it claims it is a smartphone
			preg_match('/Smartphone/', $userAgent) ||
			preg_match('/SmartPhone/', $userAgent) ||
			// Blackberry
			preg_match('/BlackBerry/', $userAgent) ||
			// Go.Web
			preg_match('/Go\.Web/', $userAgent) ||
			// Palm
			preg_match('/Palm/', $userAgent) ||
			preg_match('/palm/', $userAgent) ||
			// AvantGo
			preg_match('/AvantGo/', $userAgent) ||
			// iPhone
			preg_match('/iPhone/', $userAgent) ||
			// new Nokia phones (running on mobile OS Symbian)
			preg_match('/SymbianOS/', $userAgent) ||
			// anything that's going through Google's Transcoder trying to turn normal content into mobile suitable content (done originally for Helio)
			preg_match('/Google Wireless Transcoder/', $userAgent) ||
			// check for Opera mini or J2ME - 'cos that means a phone - based on BB 8703e production problems - see OPR-87
			preg_match('/Opera Mini/', $userAgent) ||
			preg_match('/J2ME/', $userAgent) ||
			// check for new Opera string for - HTC Touch Diamond - see OPR-521
			preg_match('/Opera Mobi/', $userAgent) ||
			// some fancy verizon phone whose full UA string is Mozilla/4.1 (compatible; MSIE 6.0; ) 400x240 LGE VX10000
			preg_match('/VX10000/', $userAgent) ||
			// LG dare - has brew in UA - using that
			preg_match('/Brew/', $userAgent) ||
			// new Google/Android phone seems to have Android in the UA - Mozilla/5.0 (Linux; U; Android 1.0; en-us; dream) AppleWebKit/525.10+ (KHTML like Gecko) Version/3.0.4 Mobile Safari/523.12.2
			preg_match('/Android/', $userAgent) ||
			// US Cellular go through a transcoder, whose UA is - Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.7) Gecko/20060909 Firefox/1.5.0.7 Novarra-Vision/7.3
			preg_match('/Novarra/', $userAgent) ||
			// webOS is Palm's latest (at time of fixing!) OS - need to send all phones with this OS to CX - see CMI-3848
			preg_match('/webOS/', $userAgent) ||
			preg_match('/brew/', $userAgent) ||
			preg_match('/BREW/', $userAgent) ||
			preg_match('/IEMobile/', $userAgent) ||	// CMI-5927 - HTC Touch pro 2 / Tilt 2
			// CMI-3367 - Nokia 7510 - hopefully Nokia don't start making PC browsers! - UA is Mozilla/5.0 ( Nokia7510a-b/2.0 (03.60) Profile/MIDP-2.1 Configuration/CLDC-1.1) AppleWebKit/420+ (KHTML, like Gecko) Safari/420+ UP.Link/6.3.1.16.0
			preg_match('/Nokia/', $userAgent)
			);
		}
		else
		{
			/*
				user agent doesn't start with Mozilla or Opera

				we want to assume that it is therefore a phone, but we want to eliminate bots, spiders etc., so the list below is based on user agents
				that were hitting the CX site before we had any tests in this section

				basically, if the user agent starts with any of these, send them to the regular HTML site (keep them away from CX).
				*/
			$returnVal =
			!(preg_match('/^NewsGatorOnline/', $userAgent) ||
			preg_match('/^Feedfetcher/', $userAgent) ||
			preg_match('/^YahooFeedSeeker/', $userAgent) ||
			preg_match('/^panscient.com/', $userAgent) ||
			preg_match('/^Java/', $userAgent) ||
			preg_match('/^PRurl/', $userAgent) ||
			preg_match('/^gsa-crawler/', $userAgent) ||
			preg_match('/^Snoopy/', $userAgent) ||
			preg_match('/^internal zero-knowledge agent/', $userAgent) ||
			preg_match('/^Baiduspider/', $userAgent) ||
			preg_match('/^RssReader/', $userAgent) ||
			preg_match('/^IEAutoDiscovery/', $userAgent) ||
			preg_match('/^AdsBot/', $userAgent) ||
			preg_match('/^AppManager/', $userAgent) ||		// CMI-1893
			preg_match('/^Wget/', $userAgent) ||		// CMI-1902
			preg_match('/^msnbot/', strtolower($userAgent)) ||		// CMI-3862
			preg_match('/^Sunrise/', $userAgent)
			);
		}
	}
	return $returnVal;
}

function getWurflDevice()
{
	/*
		most of this routine is modified versions of the demo programs supplied with the PHP libraries
	*/
	define("WURFL_DIR", $_SERVER['DOCUMENT_ROOT'] . '/wurfl/WURFL/');
	define("RESOURCES_DIR", $_SERVER['DOCUMENT_ROOT'] . '/wurfl/resources/');
	require_once WURFL_DIR . 'Application.php';
	$wurflConfigFile = RESOURCES_DIR . 'wurfl-config.xml';
	// Create WURFL Configuration from an XML config file
	$wurflConfig = new WURFL_Configuration_XmlConfig($wurflConfigFile);
	// Create a WURFL Manager Factory from the WURFL Configuration
	$wurflManagerFactory = new WURFL_WURFLManagerFactory($wurflConfig);
	// Create a WURFL Manager ($wurflManager is a WURFL_WURFLManager object)
	$wurflManager = $wurflManagerFactory->create();
	// Get information on the loaded WURFL ($wurflInfo is a WURFL_Xml_Info object)
	$wurflInfo = $wurflManager->getWURFLInfo();
	// use the whole request object (for maximum accuracy I believe!)
	$requestingDevice = $wurflManager->getDeviceForHttpRequest($_SERVER);
	return $requestingDevice;
}

/*
	determine if the three letter code associated with this user is from a deck link - if it is, return true - if it
	is not, return false.
	
	used to force links from a deck link to the mobile experience, regardless of browser
*/	
function isPhoneCode($utc)
{
	$returnVal = $utc == 'cah' ||
		$utc == 'can' ||
		$utc == 'cat' ||
		$utc == 'cbc' ||
		$utc == 'cbg' ||
		$utc == 'cgi' ||
		$utc == 'cgj' ||
		$utc == 'cgk' ||
		$utc == 'cgl' ||
		$utc == 'cgm' ||
		$utc == 'cgn' ||
		$utc == 'ckq' ||
		$utc == 'clu' ||
		$utc == 'cni' ||
		$utc == 'vzw';
	
	return $returnVal;
}	

/*
 *	getPhoneBrowserType
 *
 *	@return 0 if we don't know the type, otherwise:
 *
 *		10 - iPhone
 *		20 - Android
 *		30 - Windows Mobile
 *		40 - BlackBerry
 *
 
 
 BlackBerry
 
 Accept-Language :: en-US,en;q=0.5
 x-wap-profile :: "http://www.blackberry.net/go/mobile/profiles/uaprof/9550_80211g/5.0.0.rdf"
 Host :: cfxtest.cellfire.com
 Accept-Charset :: UTF-8,ISO-8859-1,US-ASCII,UTF-16BE,windows-1252,UTF-16LE,Shift_JIS,ISO-2022-JP,EUC-JP,EUC-KR,x-Johab,K
 SC5601,GB2312,Big5-HKSCS,Big5,windows-1250
 User-Agent :: BlackBerry9550/5.0.0.320 Profile/MIDP-2.1 Configuration/CLDC-1.1 VendorID/105
 Accept :: application/vnd.rim.html,text/html,application/xhtml+xml,application/vnd.wap.xhtml+xml,text/vnd.sun.j2me.app-descriptor,image/vnd.rim.png,image/jpeg,application/x-vnd.rim.pme.b,application/vnd.rim.ucs,image/gif;anim=1,application/vnd.rim.jscriptc;v=0-8-72,application/x-javascript,application/vnd.rim.css;v=2,text/css;media=screen,application/vnd.wap.wmlc;q=0.9,application/vnd.wap.wmlscriptc;q=0.7,text/vnd.wap.wml;q=0.7,* / *;q=0.5
 profile :: http://www.blackberry.net/go/mobile/profiles/uaprof/9550_80211g/5.0.0.rdf
 Via :: BISB_3.5.1.48, 1.1 pmds200.bisb1.blackberry:3128 (squid/2.7.STABLE7)
 Cache-Control :: max-age=259200
 Connection :: keep-alive
 
 Windows Mobile
 
 Accept :: * / *
 Accept-Language :: en-us
 UA-OS :: Windows CE (Pocket PC) - Version 5.2
 UA-color :: color16
 UA-pixels :: 480x800
 UA-Voice :: TRUE
 x-network-type :: EVDO
 UA-CPU :: ARM
 Accept-Encoding :: gzip, deflate
 User-Agent :: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; XV6975)
 Host :: cfxtest.cellfire.com
 Connection :: Keep-Alive
 Cookie :: __utma=39303857.173916244.1263250163.1263250163.1263250163.1; __utmz=39303857.1263250163.1.1.utmcsr=vzspendsmart.com|utmccn=(referral)|utmcmd=referral|utmcct=/; Persistent_id_HeR1Jz8Pq0FzPV=0B7D3BC67780:1263250168; WT_FPC=id=2e5192c9aba4cf31e751270589477357:lv=1270589477357:ss=1270589477357

 
 */
function getPhoneBrowserType()
{
	global $LOGGER;
	$returnVal = 0;
	$userAgent = $_SERVER['HTTP_USER_AGENT'];
	$allHeaders = getAllHeaders();
	// iPhone
	if (preg_match('/iphone/i', $userAgent))
	{
		$returnVal = 10;	// 10 == iPhone
	}	
	// Android
	else if (preg_match('/android/i', $userAgent))
	{
		$returnVal = 20;	// 20 == Android
	}	
	else if (preg_match('/windows phone os 7/i', $userAgent))
	{
		$returnVal = 50; // 50 == Windows Phone 7
	}
	else if (preg_match('/windows ce|smartphone/i', $userAgent))
	{
		$returnVal = 30; // 30 == Windows Mobile
	}
	else if (preg_match('/blackberry/i', $userAgent))
	{
		$returnVal = 40;	// 40 == BlackBerry
	}	
	$LOGGER->debug('getPhoneBrowserType - result is ' . $returnVal . ' UA is ' . $userAgent);
	return $returnVal;
}


/*
 *	supportsPinning
 *
 *	@return true if user agent indicates that this browser supports pinning.
 *
 *  Note: Pinning is an Explorer feature, for versions 9.0 and above
 *
 */
function supportsPinning()
{
	$retvalue = false;	
	$userAgent = $_SERVER['HTTP_USER_AGENT'];
	if (preg_match('/MSIE\s([0-9]+)\./i', $userAgent, $matches))
	{
		$retvalue = ($matches[1] >= 9);
	}
	return $retvalue;
}

function ob_to_string($someObj, $indent = "", $level = 0)
{
	// $result = "\nots - level " . $level . ' - mem usage ' . memory_get_usage() . "\n";
	$result = '';
	if ($level > 8)
	{
		$result .= 'ob_to_string reached level ' . $level . ' - not going deeper!';
	}
	else if(is_array($someObj) || is_object($someObj))
	{
		if(is_object($someObj))
		{
			$result .= $indent . get_class($someObj) . " => (\n";
		}	
		$result .= "$indent array => (\n";
		foreach($someObj as $key => $value)
		{
			if(is_object($value) || is_array($value))
			{
				$result .= $indent."\t[$key] => \n".ob_to_string($value, $indent."\t\t", ($level + 1))."\n";
			}
			else $result .= $indent."\t[$key] => $value \n";
		}
		$result .= $indent.")";
	}	
	else if(is_bool($someObj))
	{
		$result .=  $someObj?"true":"false";
	}	
	else
	{
		$result .= $someObj."";
	}	
	// $result .= "\nots - end from level " . $level . ' - mem usage ' . memory_get_usage() . "\n";
	return $result;
}

/**
 * converts an object into a DOM structure string
 *
 * @param unknown_type $someObj
 * @param DOMDocument reference $doc
 * @return unknown
 */
function ob_to_dom($someObj, DOMDocument &$doc){
	$final;
	if(is_array($someObj)){
		$final = $doc->createElement("array");
		foreach($someObj as $key => $value){
			$childNode = $doc->createElement("item");
			$childNode->setAttribute("key",$key);
			$childNode->appendChild(ob_to_dom($value, $doc));
			$final->appendChild($childNode);
		}
	}

	else if(is_object($someObj)){
		$final = $doc->createElement(get_class($someObj));
		foreach($someObj as $key => $value){
			$childNode = $doc->createElement($key);
			$childNode->appendChild(ob_to_dom($value, $doc));
			$final->appendChild($childNode);
		}
	}

	else if(is_bool($someObj))
	$final = $doc->createTextNode($someObj?"true":"false");
	else if(is_null($someObj))
	$final = $doc->createTextNode("NULL");
	else // an int or string other printable type
	$final = $doc->createTextNode($someObj);

	return $final;
}

/*
 * Converts an object into an XML string
 */
function ob_to_xml($someObj){
	$final = new DOMDocument('1.0', 'UTF-8');
	$node = ob_to_dom($someObj, $final);
	$final->appendChild($node);
	return $final->saveXML();
}

/*
 * Converts an object into an JSON string
 */
function ob_to_json($someObj){
	$result = "";
	if(is_array($someObj)) $result .= "[";
	else if(is_object($someObj)) $result .= "{";
	else if(is_bool($someObj)) return $someObj?"true":"false";
	else return '"' .preg_replace('/\"/' ,"",$someObj). '"';

	$first = true;
	foreach($someObj as $key => $value){
		if(!$first) $result .= ",";
		else $first = false; 
		if(!is_array($someObj))
			$result .= "\"$key\":".ob_to_json($value);	
		else
			$result .= ob_to_json($value);
	}

	if(is_array($someObj)) $result .= "]";
	else if(is_object($someObj)) $result .= "}";
	
	return $result;
}

/**
 * function massageAddress
 * Takes common address conventions and substitutes the proper words
 * @param String $street
 */
function massageAddress($street){
	$street = preg_replace("/\&/","and", $street);

	$replacements = array("st" => "Street",
							  "rd" => "Road",
								"ave" => "Avenue",
								"blvd" => "Boulevard",
								"wy" => "Way",
								"e" => "East",
								"w" => "West",
								"n" => "North",
								"s" => "South");

	foreach($replacements as $abrev => $fullword)
	$street = preg_replace("/( )$abrev(\.)?( |$)/","\\1$fullword\\3", $street);

	return $street;
}

/**
 * Will query the google map api to get the coordinates for an address
 *
 * @param $street
 * @param $city
 * @param $state
 * @param $zipcode
 * @return an object with members latitude and longitude
 */
function geocodeAddress($street, $city, $state, $zipcode){
	global $LOGGER, $GOOGLE_MAP_KEY;
	$LOGGER->debug("Geocoding: $street, $city, $state, $zipcode");

	$point;
	$point->latitude = 0;
	$point->longitude = 0;
	$street = massageAddress($street);
	$street = preg_replace("/\s/","+",$street);
	$city = preg_replace("/\s/","+",$city);
	$host = "http://maps.googleapis.com/maps/api/geocode/json?";
	$params = array("address=$street,+$city,+$state+$zipcode",
						"sensor=false");

	$URL = $host.(implode("&",$params));
	$LOGGER->debug("Geocoding: URL is " . $URL);
	$response = file_get_contents($URL);
	$LOGGER->debug("Geocoding: response is " . $response);
	//print_r_pre($URL);
	$json = json_decode($response);
	if (isset($json->results[0]->geometry->location))
	{
		//print_r_pre($json);
		$point->longitude = $json->results[0]->geometry->location->lng;
		$point->latitude = $json->results[0]->geometry->location->lat;
	}

	return $point;
}

/**
 * function getZipcodeFromLatLng
 * @param - lat - latitude
 * @param - lng - longitude
 *
 * Queries the zip_info table to get the closest zipcode
 */
function getZipcodeFromLatLng($lat, $lng){
	global $LOGGER;
	$LOGGER->debug('LAT = ' . $lat . ' -  LNG = ' . $lng);
	$mysql = new Mysql();
	$retVal = false;	
	if ($mysql->isNumeric($lat) && $mysql->isNumeric($lng))
	{
		// Distance formula
		$query = "select ZIP_CODE, (POW(POW(($lat-LATITUDE),2)+POW(($lng-LONGITUDE),2),.5)) as distance from zip_info order by distance limit 1 offset 0";
		$mysql->query($query);
		//echo $query;
		if($row = $mysql->getRowObject())
		{
			$retVal = $row->ZIP_CODE;
		}
	}
	$mysql->close();
	return $retVal;
}

function getQueryString($extraParams=false)
{
	global $USER;
	$queryString = '';
	if ($USER->authenticated)
	{
		$queryString = '?xb44nn2312=' . urlencode($USER->id) . (rand()%9) . (rand()%9);
	}

	foreach ($_GET as $name => $value)
	{
		if (strlen($queryString) > 0)
		{
			$queryString .= '&'.$name . '=' . urlencode($value);
		}
		else
		{
			$queryString = '?' . $name . '=' . urlencode($value);
		}
	}
	if($extraParams){	
		foreach ($extraParams as $name => $value)
		{
			if (strlen($queryString) > 0)
			{
				$queryString .= '&'.$name . '=' . urlencode($value);
			}
			else
			{
				$queryString = '?' . $name . '=' . urlencode($value);
			}
		}	
	}
	return $queryString;
}	

/*
	send email
*/
function sendMail($mailArray) 
{
	global $SMTP_SERVER,
	$SMTP_PORT,
	$SMTP_AUTH,
	$SMTP_USER,
	$SMTP_PWD,
	$LOGGER;

	/*
	 * Headers
	 */
	$headers['From'] = $mailArray['from'];
	$headers['To'] = $mailArray['to'];
	$headers['Subject'] = $mailArray['subject'];

	/*
	 * SMTP server details
	 */
	$smtp['host'] = $SMTP_SERVER;
	$smtp['port'] = $SMTP_PORT;
	$smtp['auth'] = $SMTP_AUTH;
	// $smtp['username'] = $SMTP_USER;
	// $smtp['password'] = $SMTP_PWD;

	/*
	 * MIME type settings
	 */
	$crlf = "\n";
	$mime = new Mail_mime($crlf);
	$mime->setTXTBody($mailArray['text_body']);
	$mime->setHTMLBody($mailArray['html_body']);

	$body = $mime->get();
	$headers = $mime->headers($headers);
	//print_r_pre($headers);
	//print_r_pre($smtp);
	/*
	 * Create the mail object using the Mail::factory method
	 */
	$mail =& Mail::factory('smtp', $smtp);
	 if ($mail->send($mailArray['to'], $headers, $body) === true) 
	 {
			// echo 'mail sent to ' . $mailArray['to'];
			$LOGGER->debug('sendEmail - successfully sent to ' . $mailArray['to']);
			return true;
		} 
		else 
		{
			// echo 'mail failed';
			$LOGGER->debug('sendEmail - failed to send to ' . $mailArray['to']);
			return false;
		}
	return true;
}


/*
	take the passed in ip address (in xx.xx.xx.xx format) and find the zip code of the location where that ip address
	originates.
	
	Only do this for US based IP addresses.
	
	If the initial lookup doesn't find a zip, but does find a city and state, then use the website's existing city state lookup
	to find a zip.
	
	Uses MaxMind GeoIP City Database, with a monthly subscription
	Original Invoice # for the purchase/subscription is MM_43296

	We're also using the Net_GeoIP PEAR module that directly access the binary version of the MaxMind database (see referred to below as geoIPCity.dat)
	The PEAR module consists of three files - Net/GeoIP.php, Net/GeoIP/DMA.php and Net/GeoIP/Location.php
	See http://pear.php.net/package/Net_GeoIP/docs for full details

	@return 5 digit zip code that corresponds to the location of the ip address, or -1 if the location could not be ascertained
	
	NOTE:: Currently the routine only uses zip, city and state - if we find that for certain locations, there are failing, but we are getting back
	a good longitude and latitude, this routine could be altered to use long and lat as secondary lookup rather than city state.
	
	TimH - 3/16/2009
*/
function getZipFromIp($ip)
{
  /*
	global $LOGGER, $GEO_DB_LOCATION;
	$retVal = -1;	// not found
	require_once("Net/GeoIP.php");

	$geoip = Net_GeoIP::getInstance($GEO_DB_LOCATION);
	$LOGGER->debug("getZipFromIp - looking for data file at " . $GEO_DB_LOCATION);
	try
	{
		$START_TIME = microtime(true);
		$location = $geoip->lookupLocation($ip);
		$STOP_TIME = microtime(true);
		$LOGGER->debug("PERFORMANCE ANALYSIS: ip sniffing geoip lookup: ".($STOP_TIME-$START_TIME));
		if (isset($location->countryCode) && $location->countryCode == "US")
		{
			// we only care about US locations - for now
			if (isset($location->postalCode) && strlen($location->postalCode) == 5)
			{
				$retVal = $location->postalCode;	// it found the zip
			}
			else if (isset($location->city) && isset($location->region))
			{
				$CH = new ContentHelper();
				$START_TIME = microtime(true);
				$ret = $CH->getCityStateZip($location->city, $location->region, false);
				$STOP_TIME = microtime(true);
				$LOGGER->debug("PERFORMANCE ANALYSIS: ip sniffing getting zip from city state: ".($STOP_TIME-$START_TIME));
				if (isset($ret->ZIP_CODE) && strlen($ret->ZIP_CODE) == 5)
				{
					$retVal = $ret->ZIP_CODE;
				}	
			}	
		}	
	}
	catch (Exception $e)
	{
		$LOGGER->error("getZipFromIp - Exception thrown:\n" . $e);
		$retVal = -1;
	}	
	$LOGGER->debug("ip sniffing - ip address " . $ip . " translated to zip code " . $retVal);
	return $retVal;
    */
    return 77001;
}	

/*
	getStringFromPost
	
	return either an empty string or the POSTed value for name $formName
*/	
function getStringFromPost($formName)
{
	$retVal = '';
	if (isset($_POST[$formName]) && strlen($_POST[$formName]) > 0)
	{
		$retVal = htmlspecialchars($_POST[$formName]);
	}	
	return $retVal;
}

/*
	set the passed in value in both the session and a browser session length cookie under the passed in key
*/	
function setSessionCookie($key, $value)
{
	$_SESSION[$key] = trim(htmlspecialchars($value), '/');
	// setcookie($key, trim(htmlspecialchars($value), '/'), 0, '/');
}

/*
	look for the key first in the session and secondly in a browser session length cookie, and returns its value if found
*/	
function getSessionCookie($key)
{
	$retVal = null;
	if(!empty($_SESSION[$key]))
	{
		$retVal = $_SESSION[$key];
	}
	else if(!empty($_COOKIE[$key]))
	{
		$retVal = $_COOKIE[$key];
	}
	return $retVal;
}

/*
	remove any session or cookie values assoicated with the passed in key
*/	
function clearSessionCookie($key)
{
	unset($_SESSION[$key]);
	//setcookie($key, '', -1);
}

/*
	reveal an obfuscated card number
*/	
function cReveal($obCardNumber)
{
	$ordA = ord('a');
	$rSeed = ord(substr($obCardNumber, 0 ,1)) - $ordA;	
	$nSeed = $rSeed;
	$cardNumber = '';
	for ($i = 1; $i < strlen($obCardNumber); $i += 2)
	{
		$cardNumber .= ord(substr($obCardNumber, $i , 1)) - $nSeed - $ordA;
		$nSeed += $rSeed;
		while ($nSeed > 16)
		{
			$nSeed = $nSeed - 16;
		}	
		if ($nSeed < 5)
		{
			$nSeed += 5;
		}	
	}
	return $cardNumber;
}

/*
	obfuscate a card number
*/	
function cHide($cardNumber)
{
	$rSeed = rand(5,15);
	$ordA = ord('a');
	$nSeed = $rSeed;

	$obCardNumber = chr($rSeed + $ordA);
	for ($i = 0; $i < strlen($cardNumber); $i++)
	{
		$obCardNumber .= chr($nSeed + $ordA + intval(substr($cardNumber, $i, 1)));
		$nSeed += $rSeed;
		while ($nSeed > 16)
		{
			$nSeed = $nSeed - 16;
		}	
		if ($nSeed < 5)
		{
			$nSeed += 5;
		}	
		$obCardNumber .= chr($ordA + rand(0,25));
	}		
	return $obCardNumber;
}	


// convinience class to stop php warnings
class AllCat
{
	public $categoryId;
	public $categoryName;
	public $parentCategoryId;
	public $couponIdList;
	public $isSelected;
	
	public function __construct()
	{
		
	}
	
}




?>