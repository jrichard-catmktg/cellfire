<?
/*
 * Author Jonathan Samples
 * Created February 4; 2008
 *
 * Properties File for all static keywords; and messages
 */

$APP_VERSION = "- v10.04.330";


$CVS_VERSION = '$Name:  $';


/*
 * protocol being used
 */
$SERVER_PROTOCOL = 'http';
if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) 
{

    $SERVER_PROTOCOL = 'https';
}

/*
 * ======================================================
 * Environment Settings
 * ======================================================
 */
//$myDev = "cchen";
// $myDev = "timh";
$myDev = "jonrichards";


define("IS_PRODUCTION",file_exists('/isproduction.true'));
define("IS_QA",file_exists('/isqa.true'));
define("IS_LOCAL",file_exists('/islocal.true'));

/*
	CMI-6016 - distinguish between multiple ConWeb websites running under a single Apache instance
	-- need to suck in the right properties file
*/
$multiExtension = '';
$myDocroot = $_SERVER['DOCUMENT_ROOT'];
if (preg_match('/_([0-9]+)\//', $myDocroot, $matches))
{
	$multiExtension = '_' . $matches[1];
}	

if(IS_PRODUCTION)
{
	$ENV_PROPERTY_FILE = "includes/env_properties/production" . $multiExtension . ".php";
}
else if (IS_QA)
{
	$ENV_PROPERTY_FILE = "includes/env_properties/envqa2" . $multiExtension . ".php";
}
else if (file_exists('/isdev.true'))
{
	$ENV_PROPERTY_FILE = "includes/env_properties/" . $myDev . $multiExtension . ".php";
}
else if (file_exists('/isdevcf.true'))
{
        $ENV_PROPERTY_FILE = "includes/env_properties/devcf" . $multiExtension . ".php";
}
else if (file_exists('/isdevcloud.true'))
{
	$ENV_PROPERTY_FILE = "includes/env_properties/devcloud.php";
}
else if (file_exists('/iscloud.true'))
{
	$ENV_PROPERTY_FILE = "includes/env_properties/cloud.php";
}
else if (IS_LOCAL)
{
	$ENV_PROPERTY_FILE = "includes/env_properties/local.php";
}
else if (file_exists('/isbamboo.true'))
{
        $ENV_PROPERTY_FILE = "includes/env_properties/bamboo.php";
}
else if (file_exists('/isqapod.true'))
{
        $ENV_PROPERTY_FILE = "includes/env_properties/qa-pod.php";
}
else if (file_exists('/islt.true'))
{
        $ENV_PROPERTY_FILE = "includes/env_properties/lt_env.php";
}
else if (file_exists('/isuat.true'))
{
    $ENV_PROPERTY_FILE = "env_properties/uat.php";
}
else{
	echo "<!--Error including ENV_PROPERTY_FILE;--><h1>Configuration does not exist, please contact the administrator.</h1>";
	exit();
}
include($ENV_PROPERTY_FILE);
/*
 * ========================================================
 * Applicaton Details
 * =======================================================
 */
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

//=========== Don't Change======================
$WS_CONTENT_URL = $WS_URL."ContentAccessService";
$WS_CONTENT_FILENAME = "stubs_content_access_service.php";
$WS_USER_URL = $WS_URL."UserManagementService";
$WS_USER_FILENAME = "stubs_user_management_service.php";
$WS_COM_URL = $WS_URL."UserCommunicationService";
$WS_COM_FILENAME = "stubs_user_communication_service.php";
$WS_CF_COM_URL = $WS_URL."CellfireContentService";
$WS_CF_CONTENT_FILENAME = "stubs_cellfire_content_service.php";
$WS_CF_USER_FILENAME = "stubs_cellfire_user_management_service.php";
$WS_TRACK_URL = $WS_URL."TrackingService";
$WS_TRACK_FILENAME = "stubs_user_tracking_service.php";
$WS_CF_QB_URL=$WS_URL."QBridgeService";
$WS_CF_QB_FILENAME="stubs_qbridge_service.php";

//==============================================
/*
 * General Site Details
 */
// $SITE_TITLE  = "Cellfire - Mobile coupons for your cell phone";
$SITE_TITLE  = "Grocery Coupons";

/*
 * User Information Details
 */
$USER_INFO_TITLE = "User Information";
$USER_INFO_NAME = "Name";
$USER_INFO_EMAIL = "E-Mail";
$USER_INFO_ID = "API ID";
$USER_INFO_PHONE = "Phone Number";
$USER_INFO_YOB = "Year of Birth";
$USER_INFO_ZIP = "Zip Code";
$USER_INFO_CLIPPED = "# Clipped";
$USER_INFO_CONTACTS = "# Contacts";
$USER_INFO_CARDS = "# Loyalty Cards";
$USER_INFO_OPT_EMAIL = "Email Opt-In";
$USER_INFO_OPT_SMS = "SMS Opt-In";
$USER_INFO_AUTH = "Logged In";
$USER_INFO_AUTHTIME = "Login At";
$USER_INFO_CONFIRMED = "Confirmed";
$USER_INFO_WEBNAME = "Webname";
$USER_INFO_CATEGORIES = "# Categories";
$USER_INFO_WEBCLIPID = "Web Clip ID";
$USER_INFO_PHONEID = "Phone ID";
$USER_INFO_BRANDID = "Brand ID";
$USER_INFO_PROVIDERID = "Provider ID";

$USER_LOGIN_TITLE = "Login";
$USER_LOGOUT_TITLE = "Logout";

$HELP_INDEX_TITLE = "What do you need help with?";


$MERCHANTS_TILE_TITLE = "Cellfire Merchants";
$COUPONS_TILE_TITLE = "Cellfire Deals";
$CONTACT_LIST_TITLE = "Your Cellfire Contacts";
$CLIPPED_COUPONS_TITLE = "Your Clipped Offers";
$FILTER_PREFS_TILE_TITLE = "Filter Preferences";
$CHANGE_LOCATION_TILE_TITLE = "Search for Offers";
$FEEDBACK_FORM_TITLE = "Contact Us";

$ERROR_REG_1 = "Please provide a valid US cell phone number";
$ERROR_REG_2 = "Your email address must be a valid email address";
$ERROR_REG_3 = "Phone number is required";
$ERROR_REG_4 = "Your zipcode is invalid";
$ERROR_REG_5 = "Please give us a valid year of birth";
$ERROR_REG_6 = "You have already registered.  You can already use Cellfire!";
$ERROR_LOGIN_1 = "The phone number and year of birth that you entered do not match any Cellfire user. Please try again.";
$ERROR_LOGIN_2 = "Congratulations, we have found your record.  In order to proceed we need you to verify your phone number by logging on to cellfire on your phone.";
$ERROR_LOGIN_3 = "Account not found. Please enter your registered 10 digit phone number and 4 digit year of birth to login.";
$ERROR_LOGIN_4 = "You must be at least 13 years old to use Cellfire";
$ERROR_LOGIN_5 = "We are experiencing technical difficulties.  Please try again later.";
$ERROR_LOGIN_6 = "That account has been disabled. Please contact support.";
$ERROR_CLIP_1 = "You already clipped that coupon.";
$ERROR_CLIP_2 = "You can't unclip that coupon, you haven't clipped it yet!";
$LOREM_IPSUM_NAME = "Morbi Id";
$LOREM_IPSUM_PHRASE = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam metus.";
$LOREM_IPSUM_PARA = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent sed est a elit ultrices lacinia. Sed et purus. Fusce ullamcorper sapien. Aliquam in lorem eget neque vehicula consectetuer. Vestibulum condimentum neque vitae leo. Proin ullamcorper orci dapibus lorem. Cras fringilla, enim quis accumsan fermentum, nibh nulla elementum diam, non semper sapien neque eget arcu. Sed nonummy. Integer enim. Nam non odio ut est tincidunt iaculis. Praesent tincidunt, lorem auctor semper accumsan, metus magna volutpat metus, ut varius est quam ut diam.";

$CONTACT_LIST_TITLE = "Contact List";
$ADD_CONTACT_TITLE = "Add Contact";
$UPDATE_CONTACT_TITLE = "Update Contact";

$REMOVE = "Remove";
$SELECT = "Select";

$IMPORT_CONTACT_TITLE = "Import Contact from other sites";
$SHARE_CELLFIRE_TITLE = "Share Cellfire";

$OFFER_DETAIL_TITLE = "Offer Details";

$AUTHORIZATION_FAILED_TITLE = "Authorization Failed";
$MANAGE_CONTACT_TITLE = "Manage Contact";

$support_active = true;
$SUPPORT_PHONE = "866-690-6369";
$hours = "9am and 6pm PST";
$SUPPORT_EMAIL = "support@cellfire.com";

/*
 * =============================================================
 * Filter/sort/pagination Constants
 */
$SORT_BY = "Sort by";
$FILTER_BY = "Coupons Per Category";
$RESULTS_PER_PAGE = "Results per page";
$RESULTS_PER_PAGE_OPTIONS = array(5,10,15,20);

/*
 * Phone Picker Contants
 */
$STEP_TITLES = array("Error: there is no step 0",
					"Choose your phone carrier",
					"Choose the Brand of your phone",
					"Choose the model of your phone",
					"Congratulations, cellfire supports your phone",
					"A message has been sent with instuctions");

/*
 * Button Level
 */

$ADD_CONTACT_BUTTON = "Add Contact";
$SAVE_CONTACT_BUTTON = "Save Contact";
$REMOVE_CONTACT_BUTTON = "Remove Selected Contacts";

$IMPORT_CONTACTS = "Import Contacts";

$CHECK_ALL = "Check All";
$UNCHECK_ALL = "UnCheck All";

$SAVE_CHANGES = "Save Changes";
$CANCEL = "Cancel";

/*
 * Error / Success messages (Contact Management Page)
 */
$CONTACT_ADDED = "Contact added successfully";
$ADD_CONTACT_FAILED = "Contact could not be added";

$CONTACT_UPDATED = "Contact updated successfully";
$UPDATE_CONTACT_FAILED = "Contact could not be updated";

$CONTACT_DELETED = "Selected contact(s) deleted successfully";
$DELETE_CONTACT_FAILED = "Contact could not be deleted";

$CONTACT_IMPORTED = "Contact(s) has been imported from the selected site";
$IMPORT_CONTACT_FAILED = "Contact(s) could not be imported from the selected site";

$NO_CONATCT_AVAILABLE = "No contact has been added yet";

$AUTHORIZATION_FAILED = "You are not authorization to perform this action";
$NOT_AUTHORIZED_TO_VIEW_LIST = "You are not authorization to view the list";
/*
 * Share Cellfire Page
 */
$YOUR_NAME = "Your Name";
$FRIENDS_NAME = "Friend's Name";
$FRIENDS_EMAIL = "Friend's E-Mail";
$FRIEND = "Friend";
$SHARED = "Mail sent to entered/selected person(s)";
$SHARE_FAILED = "Mail could not be sent to some of the entered/selected person(s)";
$MAIL_BODY = "Message";
$CODE = "Verification Code";
$ENTER_CODE_WRITTEN_ABOVE = "Enter code written above";
$NO_OF_FREE_MAIL_FIELDS = 5;

/*
 * Form validation error messages
 */
$NAME_NOT_ENTERED = "Name not entered";
$EMAIL_NOT_ENTERED = "E-Mail not entered";
$PHONE_NOT_ENTERED = "Phone not entered";
$INVALID_EMAIL = "Invalid E-Mail";
$MAIL_BODY_NOT_ENTERED = "E-mail Body not entered";

$USERNAME_NOT_ENTERED = "Please enter username";
$PASSWORD_NOT_ENTERED = "Please enter password";

$USER_PASSWORD_DO_NOT_MATCH = "Username and Password do not match";
$ID_NOT_TAKEN_YET = "This ID has not been taken yet";
$NO_CONTACT_TO_IMPORT = "No contact available to import";

$CODE_NOT_ENTERED = "Verification Code not entered";
$CODE_DO_NOT_MATCH = "Verification code do not match";

/*
 * Share Cellfire mail body
 */
/*
$SHARE_CELLFIRE_TEXT_MAIL_BODY = "Hi there! I found this great way to save money using my phone and wanted to share it with you. " .
								 "Cellfire is a free* service that lets you use your cell phone to get hot deals at your favorite stores and restaurants. " .
								 "Just click here to start saving!  <BR> http://www.cellfire.com' <BR><BR> * Standard carrier charges may apply";

$SHARE_CELLFIRE_HTML_MAIL_BODY = "Hi there! I found this great way to save money using my phone and wanted to share it with you. " .
								 "Cellfire is a free* service that lets you use your cell phone to get hot deals at your favorite stores and restaurants. " .
								 "Just click here to start saving!  <BR> <A target = '_blank' href = 'http://www.cellfire.com'> Link </A> <BR><BR> * Standard carrier charges may apply";

$SHARE_OFFER_HTML_MAIL_BODY = "Hi there! I just found this great deal for Hardee's and thought you might like it: FREE Jalapeno Thickburger�. Cellfire is a free* service that lets you use your cell phone to get hot deals at your favorite stores and restaurants. Just click here to start saving! <BR><BR> * Standard carrier charges may apply";

$SHARE_CELLFIRE_MAIL_SUBJECT = "Share Cellfire";
*/

$STATES = array(
		"AL" => "ALABAMA",
		"AK" => "ALASKA",
		"AZ" => "ARIZONA",
		"AR" => "ARKANSAS",
		"CA" => "CALIFORNIA",
		"CO" => "COLORADO",
		"CT" => "CONNECTICUT",
		"DE" => "DELAWARE",
		"DC" => "WASHINGTON DC",
		"FL" => "FLORIDA",
		"GA" => "GEORGIA",
		"GU" => "GUAM",
		"HI" => "HAWAII",
		"ID" => "IDAHO",
		"IL" => "ILLINOIS",
		"IN" => "INDIANA",
		"IA" => "IOWA",
		"KS" => "KANSAS",
		"KY" => "KENTUCKY",
		"LA" => "LOUISIANA",
		"ME" => "MAINE",
		"MD" => "MARYLAND",
		"MA" => "MASSACHUSETTS",
		"MI" => "MICHIGAN",
		"MN" => "MINNESOTA",
		"MS" => "MISSISSIPPI",
		"MO" => "MISSOURI",
		"MT" => "MONTANA",
		"NE" => "NEBRASKA",
		"NV" => "NEVADA",
		"NH" => "NEW HAMPSHIRE",
		"NJ" => "NEW JERSEY",
		"ME" => "NEW MEXICO",
		"NY" => "NEW YORK",
		"NC" => "NORTH CAROLINA",
		"ND" => "NORTH DAKOTA",
		"OH" => "OHIO",
		"OK" => "OKLAHOMA",
		"OR" => "OREGON",
		"PA" => "PENNSYLVANIA",
		"RI" => "RHODE ISLAND",
		"SC" => "SOUTH CAROLINA",
		"SD" => "SOUTH DAKOTA",
		"TN" => "TENNESSEE",
		"TX" => "TEXAS",
		"UT" => "UTAH",
		"VT" => "VERMONT",
		"VA" => "VIRGINIA",
		"WA" => "WASHINGTON",
		"WV" => "WEST VIRGINIA",
		"WI" => "WISCONSIN",
		"WY" => "WYOMING",
);


$LOCATIONS_IN_YOUR_AREA = "In your area";

/*
 * Feedback Page
 */
$YOUR_FEEDBACK = "Your Feedback";
$CELL_PHONE_NUMBER = "Cell Phone Number";

$WHAT_WOULD_U_DO = "What would you like to do";
$GET_HELP = "Get Help With Cellfire";
$CARD_CUSTOMER_CARE = "Get help with Cellfire and Grocery Coupons";
$KROGER_CUSTOMER_CARE = "Report a Grocery Coupon Redemption Problem";
$MAKE_SUGGESTION = "Make a Suggestion";

$KROGER_ACCOUNT_INFORMATION = "Kroger Account Information";
$KROGER_CARD_NUMBER = "Kroger Plus Card Number";

$CONTACT_INFORMATION = "Contact Information";
$FIRST_NAME = "First Name";
$LAST_NAME = "Last Name";
$ISSUE_DESC = "Issue Description";
$QUESTION_CONCERN = "Question/Concern";
$SUBJECT = "Subject";
$PHONE_INFORMATION = "Phone Information";
$CELL_SERVICE_PROVIDER = "Service Provider";
$PHONE_MANUFACTURER = "Phone Brand";
$PHONE_MODEL = "Phone Model";

$SELECT_PROVIDER = "Select provider...";
$SELECT_PHONE_BRAND = "Select phone brand...";
$SELECT_MODEL = "Select model...";


$FIRST_NAME_NOT_ENTERED = "First Name not entered";
$QUESTION_NOT_ENTERED = "Questions / Concerns not entered";
$FEEDBACK_SENT = "Feedback has been sent";
$PROVIDER_NOT_SELECTED = "Select provider";
$PHONE_NOT_SELECTED = "Select brand";
$MODEL_NOT_SELECTED = "Select model";
$SUBJECT_NOT_SELECTED = "Select subject";

$CARD_NUMBER_NOT_ENTERED = "Card number not entered";
$INVALID_CARD_NUMBER = "Invalid card number";


/*
 * Share Cellfire Page
 */
$YOUR_FEEDBACK = "Your Feedback";
$CELL_PHONE_NUMBER = "Cell Phone Number";

/*
 * Manage Account Page
 */

$PHONE_SERVICE_PROVIDER = "Phone Service Provider";
$PHONE_BRAND = "Phone Brand";
$PHONE_MODEL = "Phone Model";
$GENDER = "Gender";
$FORUM_SCREEN_NAME = "Forum Screen Name";
$ALERT_SETTINGS = "Alert Settings";
$EMAIL_ALERT = "I have agreed to receive occasional email alerts";
$SMS_ALERT = "I have agreed to receive occasional text message alerts";

$PROFILE_UPDATED = "Profile updated successfully";
$PROFILE_UPDATE_FAILED = "Profile could not be updated";

$USERNAME = "Username";
$PASSWORD = "Password";
$LOGIN_FAILED = "Authentication Failed";

/*
 * Admin - CMS Management Page
 */

$UPDATE_CMS_CONTENT_TITLE = "Update CMS Content";
$ADD_CMS_CONTENT_TITLE = "Add CMS Content";

/*
	APC constants
*/
$APC_BLITZ_COUPON = "blitz_coupon_";	// the pre-name of the key for sticking blitz coupons into the cache - has the offer id appended to it
$APC_EMPTY = "empty";	// the value use to determine that an offer was sought but not found - so that we don't constantly go back to the db for offers that don't exist
$APC_ZIP_OFFERS = "zip_offers_"; // the pre-name of the key for sticking logged out offers into the cache - has the zip code appended to it
$APC_ZIP_CATEGORIES = "zip_cats_"; // the pre-name of the key for sticking logged out offer categories into the cache - has the zip code appended to it
$APC_ALL_LOYALTY_ANONYMOUS = "all_loyalty_anon"; // the name of the user agnostic complete set of loyalty cards (for non logged in users only!)
$APC_ALL_MERCHANTS_OFFERS = "all_merchants_offers"; // the APC version of all viewable offers and their merchants - used by the website quite a bit - see content helper class getMerchants
$APC_OFFER_POOL = "offer_pool_";	// the pre-name of the key for sticking offer pool ids with the timestamp for the attached tracking code - has the tracking code appended to it

$APC_BLITZ_MERCHANT_POOL_OFFERIDS = "blitz_merchant_poids_";	// the offer ids for a given merchant/pool combo
$APC_BLITZ_ZIP_MERCHANT = "blitz_zip_merchant_";	// the id of the merchant to show initially for a given zip code
$APC_BLITZ_MERCHANT_BANNER = "blitz_zip_merchant_banner_";	// the banner image (used in blitz) id for a given merchant
$APC_BLITZ_MERCHANT_AGNOSTIC = "blitz_zip_merchant_agnostic_";	// the merchant's agnostic flag


/*
	Featured Merchants
*/	

$FEATURED_MERCHANTS = array(
548, // Safeway
444, //Kroger
545, // Best Buy
// 492, // Gamestop
515, // Ralphs
555, // Vons
538, //Sears
543, // Payless
18, //Hardee's
504, //King Soopers
496, // Caribou Coffee
4, //Hollywood
448, //Valvoline
431, //Sears Portrait
524, // Jiffy Lube
508, // Fry's
203, //Taco Bueno
476, //Stage Stores
503, //Schlotzky's
479, // peebles
478, //Palais Royal
477, // Bealls
// 480, // Arbys	
497, // KFC
505, // Baker's
498, // Firestone Auto
418, // Cincinnati Zoo
5, //1800 Flowers
507, // Dillon's
//445,//Academy for Salon Prof.
441, //Peet's Coffee & Tea
432, //McDonalds
426, //Lennys
//370, //Supercuts
//410, //Taylor Street Pizza
//427, //Una Mas
428, // La Rosa's Pizza
135, //Weinersnitzel
506, // City Market
//455, // Petoskey
45, //Subway
514, //QFC
//415, //Jones The Florist
//421, //Sabino Cycles
419, //Extreme Pita
511, // Jay C
297, //Wild Noodles
//27, //EMI Music
141, //Tony Romas
//104, //Quiznos
19, //Dominos
517, //Smith's
167, //Round Table
//417, //Kabuki Japanese
//102, //Cold Stone Creamery
//11, //Red Rock Coffee
//12, //Pizza Chicago
//6, //North Beach Pizza
597, //Giant Eagle
596, //FredMeyer
566, //shoprite
570, //Jcpenny
);

// This array maps SimpleWire Carrier Ids to the carrier keys in the conweb db.
$CARRIER_MAP = array(
87  => array("Alltel",  6),
383 => array("AT&T", 1),
7   => array("AT&T", 1),
534 => array("Boost Mobile", 	9),
586 => array("Boost Mobile", 	9),
587 => array("Cellcom", 	-1),
386 => array("Cellular South", 	7),
557 => array("Centennial", 	-1),
40  => array("Cricket Communications", 	-1),
535 => array("Dobson", 	-1),
570 => array("East Kentucky Network", 	-1),
554 => array("Farmers Wireless",-1),
516 => array("Metro PCS", 	10),
555 => array("nTelos", 	-1),
581 => array("Rural Cellular Corporation", -1),
18  => array("Sprint Nextel iDEN", 	2),
34  => array("Sprint Nextel CDMA", 	2),
384 => array("Suncom", 	11),
79  => array("T-Mobile USA", 4),
549 => array("Union Telephone", -1),
56  => array("US Cellular Corp", -1),
77  => array("Verizon", 	3),
525 => array("Virgin Mobile", -1),
559 => array("West Central Wireless", -1),	);

/*
	STORE_ICONS - used in multiMerc "pick a store" page - as they're not coming from CMS - need to do manual intervention!
*/	

$STORE_ICONS = array(
	505 => '/images/grocery/stores/Bakers_Icon.png',
	549 => '/images/grocery/stores/Carrs_Icon.png',
	506 => '/images/grocery/stores/CityMarket_Icon.png',
	507 => '/images/grocery/stores/Dillons_Icon.png',
	550 => '/images/grocery/stores/Dominicks_Icon.png',
	508 => '/images/grocery/stores/Frys_Icon.png',
	596 => '/images/grocery/stores/FredMeyer_Icon.png',
	551 => '/images/grocery/stores/Genuardis_Icon.png',
	509 => '/images/grocery/stores/Gerbes_Icon.png',
	597 => '/images/grocery/stores/GiantEagle_Icon.png',
	510 => '/images/grocery/stores/Hilander_Icon.png',
	511 => '/images/grocery/stores/JayC_Icon.png',
	504 => '/images/grocery/stores/KingSoopers_Icon.png',
	444 => '/images/grocery/stores/Kroger_Icon.png',
	512 => '/images/grocery/stores/Owens_Icon.png',
	552 => '/images/grocery/stores/Pavilions_Icon.png',
	513 => '/images/grocery/stores/Payless_Icon.png',
	514 => '/images/grocery/stores/QFC_Icon.png',
	515 => '/images/grocery/stores/Ralphs_Icon.png',
	553 => '/images/grocery/stores/Randalls_Icon.png',
	548 => '/images/grocery/stores/Safeway_Icon.png',
	516 => '/images/grocery/stores/Scotts_Icon.png',
	566 => '/images/grocery/stores/ShopRite_Icon.png',
	517 => '/images/grocery/stores/Smiths_Icon.png',
	554 => '/images/grocery/stores/Tomthumb_Icon.png',
	555 => '/images/grocery/stores/Vons_Icon.png',
);


/* 
	offer pool offer ids
	Cycle 04 2010
*/
$OFFER_POOL_IDS = array(1231, 1232, 1233, 1234, 1235, 1236, 1238, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249);
/*
	end of pool offer ids
*/	

?>