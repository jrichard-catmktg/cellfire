#!/bin/bash

echo ""
echo "# --------------------  All set! (kinda)  -------------------- #"
echo "# Run the following:"
echo""
echo "source ~/.bashrc" 
echo "phpbrew use 5.4.13"
echo "phpbrew switch 5.4.13"
echo "service apache2 restart"
echo "php -v"
echo ""
echo "# If you've already modified your host file, pull up \"http://retailer.local/info.php\" for more info."
echo ""