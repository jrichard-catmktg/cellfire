# Cellfire QMobile and QFrame Vagrant environment

This Vagrant environments sets up a local Cellfire QMobile and QFrame application.
It assumes your environment is set up to run Vagrant and VirtualBox.

1.  Clone this repository onto your local machine.
2.  Clone Cellfire's codebase into directory called "conweb" located in the /www folder.
	*  Example: git clone (cellfire's repo url) conweb
3.  Run Vagrant up.  (Wait for the setup to complete.)
    *  This will configure Apache for a mock retailer domain with an iframe that calls the Cellfire app and a domain for the raw Cellfire application.
    *  It will also create a database "conweb" with user=cwebsuer password=ROr34d to be used by Cellfire's application.
4.  For phpBrew (installs PHP 5.4.13 and the required extensions) run "bash /conf/phpbrew.sh.  This will take a few minutes.
5.  On your host machine, add two entries to your hosts file:
    *  (localhost ip) retailer.local <-- retailer's iframe
    *  (localhost ip) cf.local <-- the raw Cellfire application
    
    
# Notes

*  If you don't want to use phpBrew, the php.ini file is located in the /conf folder.
*  The /conf and /www folders are shared with the VM.
*  A dump of the conweb database is located in the /conf folder.

*  Cellfire's repository and this repository are separate.  Avoid bulk commits to their repository, double check every file.
*  This setup will allow you to start working through Cellfire's code, you'll need to add some debug code to a few of their files, which should NOT be committed to their repository.

# PHP Extensions (may be incomplete)

1.  APC
2.  GD
3.  MySql
4.  JSON
5.  SOAP
6.  GeoIP (Net?) - I used a workaround instead of installing this one.


